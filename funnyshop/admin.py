# -*- coding: utf-8 -*-
from django.contrib import admin
from imagekit.admin import AdminThumbnail
from mptt.admin import MPTTModelAdmin, TreeRelatedFieldListFilter
from funnyshop.models import Image, Category, Product


@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'admin_thumbnail',
    )
    search_fields = (
        'title',
    )
    admin_thumbnail = AdminThumbnail(image_field='image_thumb_admin')
    admin_thumbnail.short_description = 'Изображение'


class CategoryAdmin(admin.ModelAdmin):
    list_display = (
        'name',
    )
    search_fields = (
        'name',
    )


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    filter_horizontal = ('images',)
    list_display = (
        'name',
        'categories',
        'price',
        'is_public',
    )
    list_filter = (
        ('categories', TreeRelatedFieldListFilter),
        'is_public',
    )
    readonly_fields = (
        'created_at',
        'updated_at'
    )
    search_fields = (
        'name',
        'description',
        'products_webpage'
    )


admin.site.register(Category, MPTTModelAdmin)
