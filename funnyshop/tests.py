from django.test import TestCase
from django.urls import reverse
from funnyshop.models import Category, Product


class ShopViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        number_of_product = 15
        category_obj = Category.objects.create(name='Sex', parent=None)
        for product_num in range(number_of_product):
            Product.objects.create(
                name='%s' % product_num,
                categories=category_obj,
                price=product_num * 100,
                description=product_num,
                is_public=True
            )
        # For search
        Product.objects.create(
            name='Test',
            categories=category_obj,
            price=200,
            description='Меня найди большая черепаха',
            is_public=True
        )

    def test_view_url_product(self):
        resp = self.client.get('/funnyshop/product/1/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_category(self):
        resp = self.client.get('/funnyshop/category/1/')
        self.assertEqual(resp.status_code, 200)

    def test_search_is_good(self):
        resp = self.client.get('/funnyshop/search/', {'q': 'Найди меня'})
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('products' in resp.context)
        self.assertTrue(len(resp.context['products']) == 1)
