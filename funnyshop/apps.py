# -*- coding: utf-8 -*-
from django.apps import AppConfig


class FunnyshopConfig(AppConfig):
    name = 'funnyshop'
    verbose_name = 'Магазин'
