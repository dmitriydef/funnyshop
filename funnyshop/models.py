# -*- coding: utf-8 -*-
import os
import re
from uuid import uuid4
from django.db import models
from imagekit.models.fields import ImageSpecField
from imagekit.processors import ResizeToFill, ResizeToFit
from mptt.models import MPTTModel, TreeForeignKey


def get_image_path(self, filename):
    ext = filename.split('.')[-1]
    filename = '{}.{}'.format(uuid4().hex, ext)
    return os.path.join('funnyshop/pictures/', filename)


class Image(models.Model):
    title = models.CharField(
        verbose_name='Заголовок',
        max_length=255
    )
    file = models.ImageField(
        verbose_name='Файл',
        upload_to=get_image_path
    )
    image_800_600 = ImageSpecField(
        source='file',
        processors=[ResizeToFill(800, 600)],
        format='JPEG',
        options={'quality': 100}
    )
    image_1200_630 = ImageSpecField(
        source='file',
        processors=[ResizeToFill(1200, 630)],
        format='JPEG',
        options={'quality': 99}
    )
    image_thumb_320 = ImageSpecField(
        source='file',
        processors=[ResizeToFill(320, 320)],
        format='JPEG',
        options={'quality': 100}
    )
    image_thumb_admin = ImageSpecField(
        source='file',
        processors=[ResizeToFill(30, 30)],
        format='JPEG',
        options={'quality': 60}
    )
    created_at = models.DateTimeField(
        verbose_name='Дата создания',
        auto_now_add=True,
    )

    class Meta:
        ordering = [
            '-created_at',
        ]
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'

    def __str__(self):
        return '%s' % self.title


class Category(MPTTModel):
    name = models.CharField(
        verbose_name='Наименование',
        max_length=255
    )
    parent = TreeForeignKey(
        'self',
        verbose_name='Категория',
        help_text='Родительская категория.',
        on_delete=models.CASCADE,
        null=True, 
        blank=True, 
        related_name='children', 
        db_index=True
    )

    class MPTTMeta:
        order_insertion_by = ['name']

    def __str__(self):
        return '%s' % self.name

    class Meta:
        ordering = ['name']
        verbose_name = 'Категорию'
        verbose_name_plural = 'Категории'

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('funnyshop_category', kwargs={'category_id': str(self.id)})


class Product(models.Model):
    name = models.CharField(
        verbose_name='Наименование',
        max_length=255
    )
    categories = models.ForeignKey(
        Category,
        on_delete=models.CASCADE,
        verbose_name='Категория'
    )
    price = models.DecimalField(
        verbose_name='Цена', 
        max_digits=19, 
        decimal_places=2
    )
    description = models.TextField(
        verbose_name='Описание'
    )
    products_webpage = models.URLField(
        verbose_name='URL',
        help_text='Cсылка на сайт производителя.',
        null=True,
        blank=True,
    )
    images = models.ManyToManyField(
        Image,
        verbose_name='Изображения',
        blank=True,
    )
    is_public = models.BooleanField(
        verbose_name='На витрине',
        default=True,
    )
    created_at = models.DateTimeField(
        verbose_name='Создан',
        auto_now=True,
    )
    updated_at = models.DateTimeField(
        verbose_name='Отредактирован',
        auto_now=False,
        auto_now_add=True
    )

    def __str__(self):
        return '%s' % self.name
    
    class Meta:
        ordering = ['-created_at']
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('funnyshop_product', kwargs={'product_id': str(self.id)})
