# -*- coding: utf-8 -*-
from django import template
from funnyshop.models import Category


register = template.Library()


@register.inclusion_tag('funnyshop/funnyshop-category-navbar.html')
def categories_navbar():
    category_list = Category.objects.all()
    return {
        'categories': category_list
    }


@register.inclusion_tag('funnyshop/funnyshop-product-card.html')
def product_card(product):
    return {
        'product': product
    }
