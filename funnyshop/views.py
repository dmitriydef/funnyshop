# -*- coding: utf-8 -*-
from django.contrib.postgres.search import SearchVector
from django.shortcuts import get_object_or_404, render, redirect
from funnyshop.models import Category, Product


def funnyshop_home(request):
    product_list = Product.objects.filter(
        is_public=True
    )
    return render(
        request,
        'funnyshop/funnyshop-category.html',
        {
            'products': product_list
        }
    )


def funnyshop_category_home(request):
    return redirect('funnyshop_home')


def funnyshop_product_home(request):
    return redirect('funnyshop_home')


def funnyshop_category(request, category_id=None):        
    category_obj = get_object_or_404(
        Category,
        pk=category_id,
    )

    if not category_obj.parent:
        categories = category_obj.get_children()
        product_list = Product.objects.filter(
            categories__in=categories,
            is_public=True
        )
    else:
        product_list = Product.objects.filter(
            categories=category_obj,
            is_public=True
        )

    return render(
        request,
        'funnyshop/funnyshop-category.html',
        {
            'category': category_obj,
            'products': product_list,
        },
    )


def funnyshop_product(request, product_id):
    product_obj = get_object_or_404(
        Product,
        pk=product_id,
        is_public=True
    )

    breadcrumbs = product_obj.categories.get_family()

    return render(
        request,
        'funnyshop/funnyshop-product.html',
        {
            'product': product_obj,
            'breadcrumbs': breadcrumbs
        },
    )


def funnyshop_search(request):
    if 'q' in request.GET:
        search_text = request.GET['q']
        if len(search_text.replace(' ', '')) > 2 and len(search_text) <= 100:
            search_list = Product.objects.annotate(
                search=SearchVector('name', 'description', config='russian'),
            ).filter(
                search=search_text,
                is_public=True,
            )  # Remove duplicate result rows

            return render(
                request,
                'funnyshop/funnyshop-search.html',
                {
                    'search_text': search_text,
                    'products': search_list,
                }
            )

    return_path = request.META.get('HTTP_REFERER', '/')
    return redirect(return_path)



