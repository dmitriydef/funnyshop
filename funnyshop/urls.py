# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from funnyshop import views


urlpatterns = [
    url(r'^$', views.funnyshop_home, name='funnyshop_home'),
    url(r'^category/$', views.funnyshop_category_home,
        name='funnyshop_category_home'),
    url(r'^category/(?P<category_id>[0-9-_]+)/$',
        views.funnyshop_category, name='funnyshop_category'),
    url(r'^product/$', views.funnyshop_product_home,
        name='funnyshop_product_home'),
    url(r'^product/(?P<product_id>[0-9-_]+)/$',
        views.funnyshop_product, name='funnyshop_product'),
    url(r'^search/$', views.funnyshop_search, name='funnyshop_search'),
]
