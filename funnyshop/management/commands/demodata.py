# -*- coding: utf-8 -*-
import json
import random
from django.core.files import File
from django.core.files.images import ImageFile
from django.core.management.base import BaseCommand
from funnyshop.models import Category, Product, Image


demodata = {
    "categories": {
        "Бельё": {
            "Парики",
            "Маски",
            "Аксессуары"
        },
        "Фетиш": {
            "Верёвки",
            "Ошейники",
            "Стеки",
            "Наручники"
        },
    },
    'products': [
        {
            'name': 'Страстная Рыжая Лола',
            'price': random.randrange(500, 10000, 100),
            'description': 'Для создания властного и страстного образа прекрасно подойдет парик Страстная Рыжая Лола. Стрижка прямой боб подчеркнет раскрепощенность его обладательницы. Длина - 30 см. Доставка этого товара возможна не ранее, чем через день после заказа.',
            'products_webpage': 'https://www.eroshop.ru/'
        },
        {
            'name': 'Веревка Bondage Collection',
            'price': random.randrange(500, 10000, 100),
            'description': 'Веревка для связывания из коллекции Bondage от Lola Toys - прекрасный аксессуар для яркой ночи. Свяжите Вашего партнера, лишите его возможности действий. Пусть он окажется полностью в Вашей власти и будет умолять об оргазме. Нежный материал не причинит дискомфорта, а девять метров позволят Вам воплотить в жизнь любые фантазии. Длина - 9 метров, состав: полиэфир, упаковка - коробка с принтом.',
            'products_webpage': 'https://www.eroshop.ru/'
        },
        {
            'name': 'Веревка для фиксации',
            'price': random.randrange(500, 10000, 100),
            'description': 'Специальная веревка для связывания. На неё стоит потратиться, если вы занимаетесь подобными играми. Она ооочень далека от обычных "бельевых" синтетических веревок. Верёвка изготовлена из хлопка, благодаря натуральности материала и специальному тонкому плетению не натирает кожу даже при самом серьезном бондаже. Длина 10 метров. Бренд "Пикантные штучки"..',
            'products_webpage': 'https://www.eroshop.ru/'
        },
        {
            'name': 'Ошейник с хромированными заклёпками',
            'price': random.randrange(500, 10000, 100),
            'description': 'Широкий брутальный и строгий ошейник для раба. Сделан из кожзаменителя, декорирован строчкой и рядами металлических заклепок и шипов. Застегивается на замок с ключом. Имеет кольцо для крепления поводка. Длина ошейника 44, 5 см, ширина 5 см, рассчитан на обхват шеи до 41 см. Производитель "Пикантные штучки"',
            'products_webpage': 'https://yandex.ru/'
        },
        {
            'name': 'Ошейник с пряжкой',
            'price': random.randrange(500, 10000, 100),
            'description': 'Кожаный ошейник с контрастной вставкой и заклепками. Имеет три кольца. Ширина - 5 см, Размер регулируется при помощи застежки-пряжки.',
            'products_webpage': 'https://google.com/'
        },
        {
            'name': 'Кляп-трензель',
            'price': random.randrange(500, 10000, 100),
            'description': 'Кляп-трензель с жесткой перекладиной, который можно закусить во время сексуальной игры. Удобен в использовании, натуральная кожа не травмирует зубы, размер регулируется застежкой, общая длина 54 см, длина трензеля 15 см, диаметр 1,8 см.',
            'products_webpage': 'https://mail.ru/'
        },
    ]
}


class Command(BaseCommand):
    help = 'Demo data for Funny Shop.'

    def add_arguments(self, parser):
        parser.add_argument(
            '--add',
            action='store_true',
            dest='add',
            default=False,
            help='Add test data.'
        )

    def handle(self, *args, **options):
        if options['add']:
            for parent, children in demodata['categories'].items():
                category_parent = Category.objects.create(
                    name=parent, parent=None)
                for child in children:
                    category_child = Category.objects.create(name=child, parent=category_parent)
                    for x in range(random.randrange(5, 16, 1)):
                        prod = random.choice(demodata['products'])
                        newprod = Product.objects.create(
                            name=prod['name'],
                            categories=category_child,
                            price=prod['price'],
                            description=prod['description'],
                            products_webpage=prod['products_webpage'],
                            is_public=True
                        )

            print('ok')


