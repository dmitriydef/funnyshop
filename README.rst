=========
funnyshop
=========

Funny Shop - это простое приложение Django для создания небольшой витрины магазина.

Быстрый Старт
-------------

1. Скачать и установить пакет 'pip install dist/django-funnyshop-0.1.tar.gz'

2. Добавить "funnyshop" в INSTALLED_APPS::

    INSTALLED_APPS = (
        ...
        'funnyshop',
    )

3. Добавить Funny Shop URLconf в urls.py проекта::

    url(r'^funnyshop/', include('funnyshop.urls')),

4. Запустить `python manage.py migrate` для создания Funny Shop моделей.

5. Собрать статику `python manage.py collectstatic`.

6. По желанию добавить демонстрационные данные `python manage.py demodata --add`.