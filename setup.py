import os
from setuptools import setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-funnyshop',
    version='0.1',
    packages=['funnyshop'],
    include_package_data=True,
    license='BSD License',
    description='Простое приложение Django для создания небольшой витрины магазина.',
    long_description=README,
    url='http://funnytoys.californiaone.ru/funnyshop/',
    author='Dmitriy Def',
    author_email='iam@dmitriydef.ru',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',  # example license
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    install_requires=[
        'pilkit', 
        'django-imagekit',
        'django-mptt'
    ],
)
